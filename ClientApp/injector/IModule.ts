export interface IModule {
  moduleName: string;
  renderComponent: (props: any, element: string) => void;
  cachedProps: any;
  cachedElementId?: string;
}
