import { IModule } from "./IModule";

export function injectModule($class: new () => IModule) {
  let newModule = new $class();
  let oldModule = (window as any)[newModule.moduleName] as IModule | null;

  (window as any)[newModule.moduleName] = newModule;

  // The module was reloaded, likely via HMR. Rerender (props and element should still be within view)
  if (oldModule) {
    newModule.renderComponent(
      oldModule.cachedProps,
      oldModule.cachedElementId!
    );
  }
}
