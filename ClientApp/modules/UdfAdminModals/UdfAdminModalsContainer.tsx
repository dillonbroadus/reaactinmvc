import React, { useState } from "react";
import * as Reactstrap from "reactstrap";

declare function showNotification(
    message: string,
    notificationType: string
): void;

export default class UdfAdminModalsContainer extends React.Component<
    {},
    { warningIsOpen: boolean; addFieldOrCardIsOpen: boolean }
> {
    constructor(props: any) {
        super(props);

        this.state = { warningIsOpen: false, addFieldOrCardIsOpen: true };
    }

    render() {
        return (
            <>
                <WarningModal
                    isOpen={this.state.warningIsOpen}
                    toggle={() =>
                        this.setState({
                            warningIsOpen: !this.state.warningIsOpen,
                        })
                    }
                ></WarningModal>
                <AddFolder
                    isOpen={this.state.addFieldOrCardIsOpen}
                    toggle={() =>
                        this.setState({
                            addFieldOrCardIsOpen:
                                !this.state.addFieldOrCardIsOpen,
                        })
                    }
                ></AddFolder>
            </>
        );
    }
}

function WarningModal(props: { isOpen: boolean; toggle: () => void }) {
    return (
        <Reactstrap.Modal isOpen={props.isOpen}>
            <Reactstrap.ModalHeader
                close={
                    <button className="close" onClick={props.toggle}>
                        <span>×</span>
                    </button>
                }
            >
                Warning
            </Reactstrap.ModalHeader>
            <Reactstrap.ModalBody>
                Please enter 3 or more characters for search.
            </Reactstrap.ModalBody>
            <Reactstrap.ModalFooter>
                <button className="btn btn-erp" onClick={props.toggle}>
                    Close
                </button>
            </Reactstrap.ModalFooter>
        </Reactstrap.Modal>
    );
}

function applyValidationClass(element: {
    validity: ValidityState;
    classList: DOMTokenList;
}) {
    if (element.validity.valid) {
        if (element.classList.contains("is-invalid")) {
            element.classList.remove("is-invalid");
        }
        if (!element.classList.contains("is-valid")) {
            element.classList.add("is-valid");
        }
    } else {
        if (element.classList.contains("is-valid")) {
            element.classList.remove("is-valid");
        }
        if (!element.classList.contains("is-invalid")) {
            element.classList.add("is-invalid");
        }
    }
}

function checkValidationAndGetValues<T>(
    form: HTMLFormElement | null
): FormValues<T> {
    if (!form) {
        return { valid: false, values: {} };
    }

    let formIsValid = form.checkValidity();

    let resultObject = Array.from(form.elements).reduce((agg, curr) => {
        let formChild = curr as unknown as {
            attributes: NamedNodeMap;
            validity: ValidityState;
            value: string;
            classList: DOMTokenList;
        };

        let propName = formChild.attributes.getNamedItem("propname")?.value;

        if (propName) {
            agg[propName] = formChild.value;
        }

        applyValidationClass(formChild);

        return agg;
    }, {} as { [key: string]: string | null });

    return {
        valid: formIsValid,
        values: resultObject as unknown as Partial<T>,
    };
}

type UseFormValues<T> = [
    React.RefObject<HTMLFormElement>,
    (propName: keyof T) => { propname: keyof T },
    () => FormValues<T>
];

interface FormValues<T> {
    valid: boolean;
    values: Partial<T>;
}

function useForm<T>(): UseFormValues<T> {
    let formRef = React.createRef<HTMLFormElement>();

    let wasAlreadyValidated = false;

    return [
        formRef,
        (propName: keyof T) => {
            return {
                propname: propName,
            };
        },
        () => checkValidationAndGetValues<T>(formRef.current),
    ];
}

function AddFolder(props: { isOpen: boolean; toggle: () => void }) {
    let [formRef, field, checkAndGet] = useForm<{
        fieldName: string;
        displayName: string;
        sortOrder: string;
    }>();

    return (
        <Reactstrap.Modal isOpen={props.isOpen}>
            <Reactstrap.ModalHeader
                close={
                    <button className="close" onClick={props.toggle}>
                        <span>×</span>
                    </button>
                }
            >
                Add folder or card
            </Reactstrap.ModalHeader>
            <Reactstrap.ModalBody>
                <input type="hidden" id="udfParentId_AddFolder" />
                <input type="hidden" id="udfFieldType_AddFolder" />
                <input type="hidden" id="UserDefinedFieldId_AddFolder" />
                <input type="hidden" id="inUse_AddFolder" />
                <input type="hidden" id="divToReplace_AddFolder" />

                <form id="AddFolder" ref={formRef}>
                    <div
                        className="col-12 form-group"
                        id="systemFieldDiv_AddFolder"
                    >
                        <label htmlFor="systemFieldName_AddFolder">
                            Field Name
                        </label>
                        <input
                            type="text"
                            {...field("fieldName")}
                            className="form-control"
                            placeholder="Field Name"
                            title="Field Name"
                            id="systemFieldName_AddFolder"
                            required
                        />
                    </div>

                    <div className="col-12 form-group">
                        <label htmlFor="displayName_AddFolder">
                            Display Name
                        </label>
                        <input
                            type="text"
                            {...field("displayName")}
                            className="form-control"
                            placeholder="Display Name"
                            title="Display Name"
                            id="displayName_AddFolder"
                            required
                        />
                    </div>

                    <div className="col-12 form-group">
                        <label htmlFor="sortOrder_AddFolder">Sort Order</label>
                        <input
                            type="text"
                            {...field("sortOrder")}
                            pattern="[0-9]+"
                            className="form-control"
                            placeholder="Sort Order"
                            id="sortOrder_AddFolder"
                        />
                    </div>
                </form>
            </Reactstrap.ModalBody>
            <Reactstrap.ModalFooter>
                <button
                    onClick={props.toggle}
                    type="button"
                    className="btn btn-erp-alt"
                    id="cancel_AddFolder"
                >
                    Cancel
                </button>
                <button
                    type="button"
                    className="btn btn-erp"
                    id="post_AddFolder"
                    onClick={() => {
                        let formValidation = checkAndGet();

                        window.console.log(formValidation);
                    }}
                >
                    Save
                </button>
            </Reactstrap.ModalFooter>
        </Reactstrap.Modal>
    );
}
