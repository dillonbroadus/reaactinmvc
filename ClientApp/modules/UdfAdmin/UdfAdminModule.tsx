// Must be at the top for IE 11 polyfills. Can be commented out if no support needed
import "core-js";

import React from "react";
import ReactDOM from "react-dom";
import { IModule } from "../../injector/IModule";
import { injectModule } from "../../injector/injector";
import UdfAdminContainer from "./UdfAdminContainer";

// Essential module definition for embedding into page
// Feel free to extend this module if you need to support
// interacting with other components on the page
class UdfAdminModule implements IModule {
    moduleName: string = "UdfAdminModule";
    cachedProps: any;
    cachedElementId?: string;

    renderComponent(props: any, element: string) {
        this.cachedProps = props;
        this.cachedElementId = element;
        ReactDOM.render(
            <UdfAdminContainer {...props} />,
            document.getElementById(element)
        );
    }

    sayHello() {
        window.console.log(this.cachedProps.initialProp);
    }
}

// Injects the above module into the global scope of
// the page. Will also rerender the component after
// a message is received from the HMR server
injectModule(UdfAdminModule);
