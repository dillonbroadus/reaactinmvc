import React from "react";
import { testData, UdfAdminCard, UdfTable } from "./Card/UdfCard";

function times<T>(num: number, proj: (index?: number) => T) {
    let results = [];
    for (let i = 0; i < num; i++) {
        results.push(proj(i));
    }
    return results;
}

export default function UdfAdminContainer(props?: { initialProp: string }) {
    return (
        <>
            {times(5, (index) => (
                <UdfAdminCard key={index}>
                    <UdfTable udfs={testData}></UdfTable>
                </UdfAdminCard>
            ))}
        </>
    );
}
