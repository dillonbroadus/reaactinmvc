import React, { useState } from "react";
import * as Reactstrap from "reactstrap";

interface FieldType {
    Id: number;
    Name: string;
}

interface UserDefinedField {
    UserDefinedFieldId: number;
    FieldName: string;
    DisplayName: string;
    FieldType: FieldType | string;
    SortOrder: number;
    //IsInUse: boolean;
}

export const testData: UserDefinedField[] = [
    {
        UserDefinedFieldId: 2899,
        FieldName: "AdjusterPreferenceAlbanian",
        DisplayName: "Albanian",
        FieldType: "Checkbox",
        SortOrder: 1,
    },
    {
        UserDefinedFieldId: 23,
        FieldName: "AdjusterPreferenceArabic",
        DisplayName: "Arabic",
        FieldType: "Checkbox",
        SortOrder: 2,
    },
    {
        UserDefinedFieldId: 2568,
        FieldName: "AdjusterPreferenceChinese",
        DisplayName: "Chinese",
        FieldType: "Checkbox",
        SortOrder: 3,
    },
    {
        UserDefinedFieldId: 24,
        FieldName: "AdjusterPreferenceFrench",
        DisplayName: "French",
        FieldType: "Checkbox",
        SortOrder: 4,
    },
    {
        UserDefinedFieldId: 20,
        FieldName: "AdjusterPreferenceGerman",
        DisplayName: "German",
        FieldType: "Checkbox",
        SortOrder: 5,
    },
    {
        UserDefinedFieldId: 2795,
        FieldName: "AdjusterPreferenceGreek",
        DisplayName: "Greek",
        FieldType: "Checkbox",
        SortOrder: 6,
    },
    {
        UserDefinedFieldId: 2752,
        FieldName: "AdjusterPreferenceHungarian",
        DisplayName: "Hungarian",
        FieldType: "Checkbox",
        SortOrder: 7,
    },
    {
        UserDefinedFieldId: 2834,
        FieldName: "AdjusterPreferenceHausa",
        DisplayName: "Hausa",
        FieldType: "Checkbox",
        SortOrder: 8,
    },
    {
        UserDefinedFieldId: 2571,
        FieldName: "AdjusterPreferenceJapanese",
        DisplayName: "Japanese",
        FieldType: "Checkbox",
        SortOrder: 9,
    },
    {
        UserDefinedFieldId: 3199,
        FieldName: "AdjusterPreferenceHindi",
        DisplayName: "Hindi",
        FieldType: "Checkbox",
        SortOrder: 10,
    },
    {
        UserDefinedFieldId: 2849,
        FieldName: "AdjusterPreferenceIgbo",
        DisplayName: "Igbo",
        FieldType: "Checkbox",
        SortOrder: 11,
    },
    {
        UserDefinedFieldId: 2889,
        FieldName: "AdjusterPreferenceHaitian",
        DisplayName: "Haitian",
        FieldType: "Checkbox",
        SortOrder: 12,
    },
    {
        UserDefinedFieldId: 22,
        FieldName: "AdjusterPreferenceItalian",
        DisplayName: "Italian",
        FieldType: "Checkbox",
        SortOrder: 13,
    },
    {
        UserDefinedFieldId: 18,
        FieldName: "AdjusterPreferenceSpanish",
        DisplayName: "Spanish",
        FieldType: "Checkbox",
        SortOrder: 14,
    },
    {
        UserDefinedFieldId: 19,
        FieldName: "AdjusterPreferenceRussian",
        DisplayName: "Russian",
        FieldType: "Checkbox",
        SortOrder: 15,
    },
    {
        UserDefinedFieldId: 25,
        FieldName: "AdjusterPreferenceSignLanguage",
        DisplayName: "Sign Language",
        FieldType: "Checkbox",
        SortOrder: 16,
    },
    {
        UserDefinedFieldId: 21,
        FieldName: "AdjusterPreferenceVietnamese",
        DisplayName: "Vietnamese",
        FieldType: "Checkbox",
        SortOrder: 17,
    },
    {
        UserDefinedFieldId: 2569,
        FieldName: "AdjusterPreferenceMandarin",
        DisplayName: "Mandarin",
        FieldType: "Checkbox",
        SortOrder: 18,
    },
    {
        UserDefinedFieldId: 2759,
        FieldName: "AdjusterPreferencePolish",
        DisplayName: "Polish",
        FieldType: "Checkbox",
        SortOrder: 19,
    },
    {
        UserDefinedFieldId: 2888,
        FieldName: "AdjusterPreferencePortuguese",
        DisplayName: "Portuguese",
        FieldType: "Checkbox",
        SortOrder: 20,
    },
    {
        UserDefinedFieldId: 2570,
        FieldName: "AdjusterPreferenceSlovak",
        DisplayName: "Slovak",
        FieldType: "Checkbox",
        SortOrder: 21,
    },
    {
        UserDefinedFieldId: 2867,
        FieldName: "AdjusterPreferenceSwahili",
        DisplayName: "Swahili",
        FieldType: "Checkbox",
        SortOrder: 22,
    },
    {
        UserDefinedFieldId: 2882,
        FieldName: "AdjusterPreferenceThai",
        DisplayName: "Thai",
        FieldType: "Checkbox",
        SortOrder: 23,
    },
    {
        UserDefinedFieldId: 2796,
        FieldName: "AdjusterPreferenceTagalog-Phillipino",
        DisplayName: "Tagalog-Phillipino",
        FieldType: "Checkbox",
        SortOrder: 24,
    },
    {
        UserDefinedFieldId: 2833,
        FieldName: "AdjusterPreferenceTurkish",
        DisplayName: "Turkish",
        FieldType: "Checkbox",
        SortOrder: 25,
    },
    {
        UserDefinedFieldId: 2797,
        FieldName: "AdjusterPreferenceYoruba",
        DisplayName: "Yoruba",
        FieldType: "Checkbox",
        SortOrder: 26,
    },
];

export function UdfAdminCard(props: { children?: React.ReactNode }) {
    let [state, updateState] = useState({ collapsed: false });
    function toggle() {
        updateState({ collapsed: !state.collapsed });
    }

    return (
        <Reactstrap.Card>
            <Reactstrap.CardHeader className="card-header-inverse">
                <div className="row">
                    <div className="col-9 col-lg-10">
                        <h5>
                            Foreign Language
                            <a
                                id="AddUdf_10"
                                href="#"
                                className="add-field-btn"
                            >
                                <i
                                    className="far fa-plus-square text-erp"
                                    aria-hidden="true"
                                ></i>
                            </a>
                        </h5>
                    </div>

                    <div className="col-3 col-lg-2 text-right">
                        <div className="btn-group">
                            <button className="btn btn-sm btn-outline-erp sortable-edit-btn">
                                <i className="fa fa-arrows-alt-v"></i>
                            </button>
                            <button
                                className="btn btn-sm btn-outline-erp sortable-cancel-btn d-none"
                                style={{ color: "red" }}
                            >
                                <i className="fa fa-times"></i>
                            </button>
                            <Reactstrap.Button
                                className="btn-outline-erp sortable-save-btn d-none"
                                size="sm"
                                disabled
                                style={{ color: "green" }}
                            >
                                <i className="fa fa-check"></i>
                            </Reactstrap.Button>
                            <Reactstrap.Button
                                size="sm"
                                className="btn-outline-erp"
                                onClick={toggle}
                            >
                                {state.collapsed ? (
                                    <i className="fas fa-chevron-down"></i>
                                ) : (
                                    <i className="fas fa-chevron-up"></i>
                                )}
                            </Reactstrap.Button>
                        </div>
                    </div>
                </div>
            </Reactstrap.CardHeader>
            <Reactstrap.Collapse isOpen={!state.collapsed}>
                <Reactstrap.CardBody>{props.children}</Reactstrap.CardBody>
            </Reactstrap.Collapse>
        </Reactstrap.Card>
    );
}

export function UdfTable(props: { udfs: UserDefinedField[] }) {
    return (
        <div className="table-responsive">
            <table className="table table-erp table-striped table-hover text-nowrap">
                <thead>
                    <tr>
                        <th>Id </th>
                        <th>Field Name </th>
                        <th>Display Name </th>
                        <th>Field Type </th>
                        <th>Sort Order </th>
                    </tr>
                </thead>
                <tbody>
                    {props.udfs.map((udf) => (
                        <tr key={udf.UserDefinedFieldId}>
                            <td>{udf.UserDefinedFieldId}</td>
                            <td>{udf.FieldName}</td>
                            <td>{udf.DisplayName}</td>
                            <td>{udf.FieldType}</td>
                            <td>{udf.SortOrder}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}
