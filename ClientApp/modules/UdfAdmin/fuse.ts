import { fusebox } from "fuse-box";
import { IRunProps } from "fuse-box/config/IRunProps";
import path from "path";

export function buildFuse(currentPath: string) {
    let fuse = fusebox({
        entry: path.join(currentPath, "UdfAdminModule.tsx"),
        target: "browser",
        devServer: {
            httpServer: false,
            hmrServer: {
                enabled: true,
                connectionURL: "ws://localhost:4444",
            },
        },
        watcher: {
            enabled: true,
            root: [
                process.cwd(),
                path.join(path.dirname(process.cwd()), "shared"),
            ],
        },
    });

    let bundleInfo: IRunProps = {
        bundles: {
            distRoot: "../Scripts/ClientApp/dist/UdfAdminModule",
            app: "bundle.js",
        },
        buildTarget: "ES3",
    };

    return {
        runProd: () => fuse.runProd(bundleInfo),
        runDev: () => fuse.runDev(bundleInfo),
    };
}
