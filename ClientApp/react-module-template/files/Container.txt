import React from "react";

export default function <%= containerComponentName %>() {
    return <div><%= containerComponentName %> works!</div>;
}