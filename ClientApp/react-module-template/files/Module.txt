// Must be at the top for IE 11 polyfills. Can be commented out if no support needed
import "core-js/stable";

import React from "react";
import ReactDOM from "react-dom";
import { IModule } from "../../injector/IModule";
import { injectModule } from "../../injector/injector";
import <%= containerComponentName %> from "./<%= containerComponentName %>";

// Essential module definition for embedding into page
// Feel free to extend this module if you need to support
// interacting with other components on the page
class <%= moduleName %> implements IModule {
    moduleName: string = "<%= moduleName %>";
    cachedProps: any;
    cachedElementId?: string;
    renderComponent(props: any, element: string) {
        this.cachedProps = props;
        this.cachedElementId = element;
        ReactDOM.render(
            <<%= containerComponentName %> {...props} />,
            document.getElementById(element)
        );
    }
}

// Injects the above module into the global scope of
// the page. Will also rerender the component after
// a message is received from the HMR server
injectModule(<%= moduleName %>);

