const path = require("path");
// TODO: Should probably not assume it's always the second file
let modulesDir = path.join(path.dirname(module.paths[1]), "modules");
let name = process.argv[2];

let yeoman = require("yeoman-environment");
var Generator = require("yeoman-generator");
let env = yeoman.createEnv();

env.registerStub(
    class extends Generator {
        constructor(args, options) {
            super(args, options);
            this.sourceRoot(path.dirname(module.paths[0]));
            this.templ;
        }

        generateNewModule() {
            if (name === null || name === undefined) {
                this.log.error(
                    `No module name provided! Please run the script again with a valid module name as an argument (e.g. Important)`
                );
                return;
            }

            this.destinationRoot(path.join(modulesDir, name));

            let containerComponentName = `${name}Container`;
            let containerComponentDestination = this.destinationPath(
                `${containerComponentName}.tsx`
            );

            let moduleName = `${name}Module`;
            let moduleDestination = this.destinationPath(`${moduleName}.tsx`);

            let fuseDestination = this.destinationPath("fuse.ts");

            this.fs.copyTpl(
                this.templatePath("files", "Container.txt"),
                containerComponentDestination,
                {
                    containerComponentName: containerComponentName,
                    moduleName: moduleName,
                }
            );
            this.fs.copyTpl(
                this.templatePath("files", "Module.txt"),
                moduleDestination,
                {
                    containerComponentName: containerComponentName,
                    moduleName: moduleName,
                }
            );
            this.fs.copyTpl(
                this.templatePath("files", "fuse.txt"),
                fuseDestination,
                {
                    containerComponentName: containerComponentName,
                    moduleName: moduleName,
                }
            );
        }
    },
    "react-module"
);

env.run("react-module");
