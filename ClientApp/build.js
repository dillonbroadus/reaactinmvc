const path = require("path");
const recursive = require("recursive-readdir");
process.chdir(process.env.INIT_CWD);

require("ts-node").register({
  transpileOnly: true,
  cwd: process.env.INIT_CWD,
});

function getFuse(fusePath) {
  return require(fusePath).buildFuse(path.dirname(fusePath));
}

if (process.argv[2] === "--all") {
  let baseDir = path.join(path.dirname(process.argv[1]), "modules");

  console.log("Scanning for fuses...");
  recursive(baseDir).then((files) => {
    let fuses = files.filter((file) => path.basename(file) === "fuse.ts");

    if (fuses.length === 0) {
      console.log();
      console.log(
        "\x1b[41m%s\x1b[0m",
        `No fuses found in ${baseDir} or any of its children`
      );
      console.log();
    } else {
      console.log();
      console.log("Building the following fuses:");
      fuses.forEach((fuse) => console.log(fuse));
      console.log();
      console.log();

      Promise.all(fuses.map((fuse) => getFuse(fuse).runProd())).then(() =>
        process.exit()
      );
    }
  });
} else {
  const fuse = getFuse(path.join(process.env.INIT_CWD, "fuse.ts"));

  switch (process.argv[2]) {
    case "--dev":
      fuse.runDev();
      break;

    case "--prod":
      fuse.runProd().then(() => process.exit());
      break;

    default:
      throw "Inavlid arguments. Supported arguments are: --debug [default], --prod, --all";
  }
}
